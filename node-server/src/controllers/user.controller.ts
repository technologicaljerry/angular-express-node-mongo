import { Request, Response } from "express";

export const createUser = async (req: Request, res: Response) => {
  console.log("Signup Request Data in user controller:", req.body);

  const { firstName, lastName, email, password, role } = req.body;

  if (!firstName) {
    return res
      .status(422)
      .json({ message: "The fields firstName is required" });
  } else if (!lastName) {
    return res.status(422).json({ message: "The fields lastName is required" });
  } else if (!email) {
    return res.status(422).json({ message: "The fields email is required" });
  } else if (!password) {
    return res.status(422).json({ message: "The fields password is required" });
  } else if (!role) {
    return res.status(422).json({ message: "The fields role is required" });
  }
};
