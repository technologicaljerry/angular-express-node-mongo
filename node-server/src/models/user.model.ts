import mongoose, { Schema, Model, Document } from "mongoose";

export interface UserInput {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  companyName: string;
  role: string;
}

export interface UserDocument extends UserInput, mongoose.Document {
  createdAt: Date;
  updatedAt: Date;
  comparePassword(candidatePassword: string): Promise<Boolean>;
}

const userSchema = new mongoose.Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    companyName: { type: String, required: true },
    role: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);
