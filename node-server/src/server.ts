import express, { Request, Response } from "express";
import dotenv from "dotenv";
import config from "config";
import { userRoute } from "./routes/user.routes";

dotenv.config();

const HOST = process.env.HOST || "http://localhost";

const PORT = parseInt(process.env.PORT || "3030");

const app = express();

app.use(express.urlencoded({ extended: true }));

app.use(express.json());

app.use("/", userRoute());

app.get("/", (req, res) => {
  return res.json({ message: "Express Server is running!" });
});

app.listen(PORT, async () => {
  console.log(`Server started aand runs on  => ${HOST}:${PORT}`);
});
